package local.misc.runchisesimpleapi.service;

import local.misc.runchisesimpleapi.GenericSpecification;
import local.misc.runchisesimpleapi.SearchCriteria;
import local.misc.runchisesimpleapi.model.Restaurant;
import local.misc.runchisesimpleapi.repository.RestaurantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RestaurantService {
    @Autowired
    private RestaurantRepo repo;

    public Restaurant findById(Integer id) {
        return repo.findById(id).get();
    }

    public List<Restaurant> findByAttribute(@RequestParam Map<String, String> params) {
        if (params.isEmpty()) {
            return repo.findAll();
        }

        GenericSpecification<Restaurant> specs = new GenericSpecification<>();
        if (params.containsKey("name")) {
            specs.add(new SearchCriteria("name", params.get("name")));
        }
        if (params.containsKey("email")) {
            specs.add(new SearchCriteria("email", params.get("email")));
        }
        if (params.containsKey("phone")) {
            specs.add(new SearchCriteria("phone", params.get("phone")));
        }
        if (params.containsKey("address")) {
            specs.add(new SearchCriteria("address", params.get("address")));
        }

        return repo.findAll(specs);
    }

    public Restaurant save(@Nullable Integer id, Restaurant data) {
        Restaurant res;
        if (id != null) {
            res = repo.findById(id).get();
            res.setName(data.getName());
            res.setEmail(data.getEmail());
            res.setAddress(data.getAddress());
            res.setPhoneNumber(data.getPhoneNumber());
        } else {
            res = data;
        }

        return repo.save(res);
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
