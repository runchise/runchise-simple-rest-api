# runchise-simple-rest-api
Create a basic REST web server with a database (any database). Create An API for
Runchise to:
- create a new restaurant
- show detail a restaurant (name, email, phone number, address) by ID
- update restaurant's information (name, email, phone number, address)
- delete a restaurant by ID
- find restaurant(s) by (name, address, email, and phone number can be 1 or more
field)
- create example unit / integration test for this API

# Environtment Setup
1. import `Runchise.postman_collection.json` to Postman
2. create local database
3. setup `resources/application.properties` file with
```properties
spring.datasource.url=jdbc:mysql://localhost:3306/[db_name]
spring.datasource.username=[db_user]
spring.datasource.password=[db_pass]
```
5. create restaurant table
```mysql
create table restaurants
(
    id      int auto_increment
        primary key,
    name    varchar(50) not null,
    email   varchar(50) not null,
    phone   varchar(12) not null,
    address text        not null
);
```
7. import `runchise_restaurants.csv` to restaurant table in local database

# How to Run
* run `./gradlew bootRun`
* run unit test `./gradlew test`

### Reference Documentation

For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.2/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.2/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.2/reference/htmlsingle/#web)
* [Spring Data JDBC](https://docs.spring.io/spring-boot/docs/2.7.2/reference/htmlsingle/#data.sql.jdbc)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.2/reference/htmlsingle/#data.sql.jpa-and-spring-data)

### Guides

The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Using Spring Data JDBC](https://github.com/spring-projects/spring-data-examples/tree/master/jdbc/basics)
* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Additional Links

These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)
