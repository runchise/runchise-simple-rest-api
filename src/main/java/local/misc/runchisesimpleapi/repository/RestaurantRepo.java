package local.misc.runchisesimpleapi.repository;

import local.misc.runchisesimpleapi.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RestaurantRepo extends JpaRepository<Restaurant, Integer>, JpaSpecificationExecutor<Restaurant> {
}