package local.misc.runchisesimpleapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RunchiseSimpleApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RunchiseSimpleApiApplication.class, args);
    }

}
