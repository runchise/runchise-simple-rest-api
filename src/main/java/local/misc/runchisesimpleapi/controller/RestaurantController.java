package local.misc.runchisesimpleapi.controller;

import local.misc.runchisesimpleapi.model.Restaurant;
import local.misc.runchisesimpleapi.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {
    @Autowired
    RestaurantService svc;

    @GetMapping(value = "")
    public ResponseEntity<List<Restaurant>> getAll(@RequestParam Map<String, String> parameters) {
        List<Restaurant> res = svc.findByAttribute(parameters);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Restaurant> get(@PathVariable Integer id) {
        Restaurant res = svc.findById(id);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Restaurant> add(@RequestBody Restaurant data) {
        return new ResponseEntity<>(svc.save(null, data), HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Restaurant> update(@PathVariable Integer id, @RequestBody Restaurant data) {
        return new ResponseEntity<>(svc.save(null, data), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id) {
        svc.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
