package local.misc.runchisesimpleapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import local.misc.runchisesimpleapi.controller.RestaurantController;
import local.misc.runchisesimpleapi.model.Restaurant;
import local.misc.runchisesimpleapi.service.RestaurantService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RestaurantController.class)
public class RestaurantControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;

    @MockBean
    RestaurantService svc;

    Restaurant RECORD_1 = new Restaurant(1, "Warung Bensu", "warbensu@gmail.com", "08122323132", "jakarta");
    Restaurant RECORD_2 = new Restaurant(2, "Ayam Geprek Juara", "juaraayam@gmail.com", "08129381378", "jakarta timur");

    @Test
    public void getAllRecordsWithEmptyParams_success() throws Exception {
        List<Restaurant> records = new ArrayList<>(Arrays.asList(RECORD_1, RECORD_2));

        Map<String, String> params = new HashMap<>();
        Mockito.when(svc.findByAttribute(params)).thenReturn(records);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/restaurants")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is("Warung Bensu")));
    }

    @Test
    public void getAllRecordsWithParams_success() throws Exception {
        List<Restaurant> records = new ArrayList<>(List.of(RECORD_1));

        Map<String, String> params = new HashMap<>();
        params.put("name", "warung bensu");
        Mockito.when(svc.findByAttribute(params)).thenReturn(records);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/restaurants")
                        .param("name", "warung bensu")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("Warung Bensu")));
    }

    @Test
    public void getRecordsWithID_success() throws Exception {
        Mockito.when(svc.findById(RECORD_1.getId())).thenReturn(RECORD_1);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/restaurants/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.name", is("Warung Bensu")));
    }

    @Test
    public void getRecordsWithID_fail() throws Exception {
        Mockito.when(svc.findById(RECORD_1.getId())).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/restaurants/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createRecord_success() throws Exception {
        Restaurant record = RECORD_1;

        Mockito.when(svc.save(null, record)).thenReturn(record);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/restaurants/create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(record));

        MvcResult result = mockMvc.perform(mockRequest).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void updateRecord_success() throws Exception {
        Restaurant record = RECORD_1;
        record.setName("Warung Bensu New");

        Mockito.when(svc.save(1, record)).thenReturn(record);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/restaurants/update/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(record));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }

    @Test
    public void deletePatientById_success() throws Exception {
        Mockito.when(svc.findById(RECORD_2.getId())).thenReturn(RECORD_2);

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/restaurants/delete/2")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
